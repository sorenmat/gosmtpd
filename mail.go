package main

import (
	"errors"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/pborman/uuid"
	"gitlab.com/sorenmat/gosmtpd/model"
	"gitlab.com/sorenmat/gosmtpd/server"
)

func processClientRequest(mc *model.MailConnection, server *server.MailServer) {
	defer mc.Connection.Close()
	id := uuid.New()
	greeting := "220 " + server.Hostname + " SMTP goSMTPd #" + id + " " + time.Now().Format(time.RFC1123Z)
	for i := 0; i < 100; i++ {
		switch mc.State {
		case model.INITIAL:
			answer(mc, greeting)
			mc.State = model.NORMAL
		case model.NORMAL:
			handleNormalMode(mc)
		case model.READ_DATA:
			var err error
			mc.Data, err = readFrom(mc)
			if err == nil {
				if status := server.SaveMail(mc); status {
					answer(mc, "250 model.OK : queued as "+id)
				} else {
					answer(mc, "554 Error: transaction failed.")
				}
			} else {
				log.Printf("DATA read error: %v\n", err)
			}
			mc.State = model.NORMAL
		}

		cont := mc.Flush()
		if !cont {
			return
		}
	}
}

func handleNormalMode(mc *model.MailConnection) {
	line, err := readFrom(mc)
	if err != nil {
		return
	}

	switch {

	case isCommand(line, model.HELLO):
		if len(line) > 5 {
			mc.Helo = line[5:]
		}
		answer(mc, "250 "+*hostname+" Hello ")

	case isCommand(line, model.EHLO):
		if len(line) > 5 {
			mc.Helo = line[5:]
		}
		answer(mc, "250-"+*hostname+" Hello "+mc.Helo+"["+mc.Address+"]"+"\r\n")
		answer(mc, "250-SIZE "+strconv.Itoa(mailMaxSize)+"\r\n")
		answer(mc, "250 HELP")

	case isCommand(line, model.MAIL_FROM):
		if len(line) > 10 {
			mc.From = line[10:]
		}
		answer(mc, model.OK)

	case isCommand(line, model.RCPT_TO):
		if len(line) > 8 {
			mc.Recepient = append(mc.Recepient, line[8:])
			mc.To = line[8:]
		}
		answer(mc, model.OK)

	case isCommand(line, model.DATA):
		answer(mc, "354 Start mail input; end with <CRLF>.<CRLF>")
		mc.State = model.READ_DATA

	case isCommand(line, model.NO_OP):
		answer(mc, model.OK)

	case isCommand(line, model.RESET):
		mc.From = ""
		mc.To = ""
		answer(mc, model.OK)

	case isCommand(line, "QUIT"):
		answer(mc, "221 Goodbye")
		killClient(mc)

	default:
		answer(mc, "500 5.5.1 Unrecognized command.")
	}
}

func answer(mc *model.MailConnection, line string) {
	mc.Response = line + "\r\n"
}

func killClient(mc *model.MailConnection) {
	mc.DropConnection = true
}

func getTerminateString(state model.State) string {
	terminateString := "\r\n"
	if state == model.READ_DATA {
		// DATA state
		terminateString = "\r\n.\r\n"
	}
	return terminateString

}

func readFrom(mc *model.MailConnection) (input string, err error) {
	var read string

	terminateString := getTerminateString(mc.State)

	for err == nil {
		mc.ResetDeadLine()
		read, err = mc.NextLine()

		if err != nil {
			break
		}

		if read != "" {
			input = input + read
			if len(input) > mailMaxSize {
				err = errors.New("DATA size exceeded (" + strconv.Itoa(mailMaxSize) + ")")
				return input, err
			}

			if mc.InDataMode() {
				scanForSubject(&mc.Mail, read)
			}
		}
		if strings.HasSuffix(input, terminateString) {
			break
		}
	}
	return input, err
}

func scanForSubject(mc *model.Mail, line string) {
	if mc.Subject == "" && strings.Index(strings.ToUpper(line), model.SUBJECT) == 0 {
		mc.Subject = line[9:]
	} else if strings.HasSuffix(mc.Subject, "\r\n") {
		// remove the newline stuff
		mc.Subject = mc.Subject[0 : len(mc.Subject)-2]
		if (strings.HasPrefix(line, " ")) || (strings.HasPrefix(line, "\t")) {
			// multiline subject
			mc.Subject = mc.Subject + line[1:]
		}
	}
}
