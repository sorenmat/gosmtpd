package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"github.com/jhillyerd/enmime"
	"gitlab.com/sorenmat/gosmtpd/model"
	"gitlab.com/sorenmat/gosmtpd/server"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func setupWebRoutes(config *server.MailServer, e *echo.Echo) {
	e.Any("/inbox/:email/listen", registerListener(config))
	e.GET("/status", status)
	e.GET("/inbox", allMails(config))
	e.GET("/inbox/:email", inbox(config))
	e.GET("/inbox/:email/listen", registerListener(config))
	e.GET("/inbox/:email/:id", mailByID(config))

	e.DELETE("/inbox/:email", deleteMails(config))
	e.DELETE("/inbox/:email/:id", deleteByID(config))
	e.DELETE("/inbox", deleteAllMails(config))
}

func getEmailFromListenURL(url string) string {
	r, _ := regexp.Compile(`\/inbox\/(.*)\/listen`)
	email := r.FindStringSubmatch(url)
	if len(email) != 2 {
		return ""
	}
	return email[1]
}

// Register a listener for changes to the mail database
func registerListener(config *server.MailServer) echo.HandlerFunc {
	return func(c echo.Context) error {
		r := c.Request()
		w := c.Response()

		sinceStr := c.FormValue("since")

		email := getEmailFromListenURL(r.URL.String())

		listener := make(chan model.MailConnection)

		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
			return err
		}
		id := uuid.New().String()
		defer func() {
			config.RemoveListener(id)
			conn.Close()
		}()
		config.RegisterListener(id, listener)

		// if sinceStr is present fetch data from database and
		// publish those first
		if sinceStr != "" {
			i, err := strconv.ParseInt(sinceStr, 10, 64)
			if err != nil {
				panic(err)
			}
			//config.Mu.Lock()

			for _, msg := range config.Database.Find(email) {
				if msg.Received >= i {
					d, err := json.Marshal(msg)
					if err != nil {
						c.Logger().Error(err)
					}

					err = conn.WriteMessage(websocket.TextMessage, d)

					if err != nil {
						c.Logger().Error(err)
					}
				}
			}
			//config.Mu.Unlock()
		}

		go func() {
			for {
				messageType, _, err := conn.ReadMessage()
				if err != nil {
					return
				}
				if messageType == websocket.CloseMessage {
					conn.Close()
					break
				}
			}
		}()
		for {
			msg := <-listener
			if msg.To == email {
				err = conn.WriteJSON(msg)
				if err != nil {
					// This is most likely because the client closed the connection
					c.Logger().Error(err)
				}
			}
		}
	}
}

func status(c echo.Context) error {
	return c.String(http.StatusOK, "OK")
}

func allMails(config *server.MailServer) echo.HandlerFunc {
	return func(c echo.Context) error {
		if strings.Contains(c.Request().Header.Get("Accept"), "text/html") {
			return c.HTML(http.StatusOK, toHTML(config.Database.All()))
		}
		return c.JSON(http.StatusOK, config.Database.All())

	}
}

func toHTML(mails []model.Mail) string {
	result := ""
	if len(mails) == 0 {
		result += "<h3>No emails in inbox</h3>"
		return result
	}
	for _, v := range mails {
		result += fmt.Sprintf("<h3>To: %v</h3>", template.HTMLEscapeString(v.To))
		result += fmt.Sprintf("<h3>Subject:%v</h3>", template.HTMLEscapeString(v.Subject))
		env, err := enmime.ReadEnvelope(bytes.NewBufferString(v.Data))
		if err != nil {
			fmt.Print(err)
			return ""
		}
		result += fmt.Sprintf("%v", template.HTMLEscapeString(env.HTML))
		result += "<hr>"
	}
	return result
}

func inbox(config *server.MailServer) echo.HandlerFunc {
	return func(c echo.Context) error {
		email := c.Param("email")
		if email == "" {
			return echo.NewHTTPError(http.StatusBadRequest, "email is required")
		}
		var result = config.Database.Find(email)

		if len(result) == 0 {
			return echo.NewHTTPError(http.StatusNotFound)

		}
		return c.JSON(http.StatusOK, result)

	}
}

func mailByID(config *server.MailServer) echo.HandlerFunc {
	return func(c echo.Context) error {
		id := c.Param("id")
		if id == "" {
			return echo.NewHTTPError(http.StatusBadRequest, "id is required")
		}

		m, err := config.Database.Get(id)
		if err != nil {
			return echo.NewHTTPError(http.StatusNotFound)

		}
		return c.JSON(http.StatusOK, m)

	}
}

func deleteMails(config *server.MailServer) echo.HandlerFunc {
	return func(c echo.Context) error {
		email := c.Param("email")
		if email == "" {
			return echo.NewHTTPError(http.StatusBadRequest, "email is required")
		}
		config.Database.Delete(email)
		return echo.NewHTTPError(http.StatusNoContent)
	}
}

func deleteAllMails(config *server.MailServer) echo.HandlerFunc {
	return func(c echo.Context) error {
		config.Database.Empty()
		return echo.NewHTTPError(http.StatusNoContent)

	}
}

func deleteByID(config *server.MailServer) echo.HandlerFunc {
	return func(c echo.Context) error {
		id := c.Param("id")
		if id == "" {
			return echo.NewHTTPError(http.StatusBadRequest, "id is required")
		}
		config.Database.DeleteByID(id)
		return echo.NewHTTPError(http.StatusNoContent)

	}
}
