package server

import (
	"testing"

	"gitlab.com/sorenmat/gosmtpd/server"
)

func TestIfItForwardEmail(t *testing.T) {
	var mailServer = server.NewMailServer(MailServer{Port: "2525",
		Httpport:          "8000",
		ForwardEnabled:    true,
		ForwardHostFilter: true,
		ForwardHost:       "test.com",
		ForwardPort:       "465",
		Expireinterval:    1,
		Ssl:               true,
		Sslcert:           "example.crt",
		Sslkey:            "example.key",
	})

	shouldForward := mailServer.shouldForwardEmail("test@test.com")

	if shouldForward != true {
		t.Error("Should be true")
	}
}

func TestIfItForwardEmailWithWrongEmail(t *testing.T) {
	var mailServer = server.NewMailServer(MailServer{Port: "2525",
		Httpport:          "8000",
		ForwardEnabled:    true,
		ForwardHostFilter: true,
		ForwardHost:       "test.com",
		ForwardPort:       "465",
		Expireinterval:    1,
		Ssl:               true,
		Sslcert:           "example.crt",
		Sslkey:            "example.key",
	})

	shouldForward := mailServer.shouldForwardEmail("test@testme.com")

	if shouldForward != false {
		t.Error("Should be false")
	}
}

func TestIfItDoesntForwardEmail(t *testing.T) {
	var mailServer = server.NewMailServer(MailServer{Port: "2525",
		Httpport:          "8000",
		ForwardEnabled:    true,
		ForwardHostFilter: false,
		ForwardHost:       "test.com",
		ForwardPort:       "465",
		Expireinterval:    1,
		Ssl:               true,
		Sslcert:           "example.crt",
		Sslkey:            "example.key",
	})

	shouldForward := mailServer.shouldForwardEmail("test@test1.com")

	if shouldForward != true {
		t.Error("Should be true")
	}
}
