package server

import (
	"testing"
)

func TestCleanUpEmail(t *testing.T) {
	email := "<sorenm@mymessages.dk>"
	ce, err := CleanupEmail(email)
	if err != nil {
		t.Error(err)
	}
	if ce != "sorenm@mymessages.dk" {
		t.Error("Address is wrong, when cleaning emails")
	}
}
