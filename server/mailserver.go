package server

import (
	"log"
	"net/mail"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
	"gitlab.com/sorenmat/gosmtpd/database"
	"gitlab.com/sorenmat/gosmtpd/model"
)

// MailServer is the main configuration structure of the program
// holds configuration and state
type MailServer struct {
	Hostname string
	Port     string

	ForwardEnabled    bool
	ForwardHostFilter bool
	ForwardHost       string
	ForwardPort       string
	ForwardUser       string
	ForwardPassword   string

	Debug bool
	// In-Memory Database
	Database database.DataStore
	mu       *sync.Mutex

	Httpport string
	Ssl      bool
	Sslcert  string
	Sslkey   string

	ExpireMutex    *sync.Mutex
	Expireinterval int

	listenersMutex *sync.Mutex
	listeners      map[string]chan model.MailConnection
}

func NewMailServer(server MailServer) *MailServer {
	server.mu = &sync.Mutex{}
	server.ExpireMutex = &sync.Mutex{}
	server.listenersMutex = &sync.Mutex{}
	server.listeners = make(map[string]chan model.MailConnection)
	return &server
}
func (server *MailServer) RegisterListener(id string, listener chan model.MailConnection) {
	server.listenersMutex.Lock()
	server.listeners[id] = listener
	server.listenersMutex.Unlock()
}

func (server *MailServer) RemoveListener(id string) {
	server.listenersMutex.Lock()
	delete(server.listeners, id)
	server.listenersMutex.Unlock()
}

func (server *MailServer) NotifyListeners(mc model.MailConnection) {
	server.listenersMutex.Lock()
	for _, v := range server.listeners {
		v <- mc // notify all listener
	}
	server.listenersMutex.Unlock()
}
func (server *MailServer) SaveMail(mc *model.MailConnection) bool {
	server.mu.Lock()
	defer server.mu.Unlock()

	if server.Debug {
		log.Println(mc.Mail)
	}
	if err := isEmailAddressesValid(&mc.Mail); err != nil {
		log.Printf("Email from '%v' doesn't have a valid email address the error was %v\n", mc.From, err)
		return false
	}
	mc.ExpireStamp = time.Now().Add(time.Duration(server.Expireinterval) * time.Second)
	mc.Received = time.Now().Unix()

	// Save the mail in each recipient mailbox
	for _, rcpt := range mc.Recepient {
		to, err := CleanupEmail(rcpt)
		if err != nil {
			log.Printf("Cleaning up email gave an error %v\n", err)
		}
		mc.To = to
		mc.MailId = uuid.New().String()

		server.Database.Save(mc.Mail)
	}
	server.NotifyListeners(*mc)

	if server.ForwardHost != "" && server.ForwardPort != "" {
		if server.shouldForwardEmail(mc.To) {
			server.ForwardEmail(&mc.Mail)
		}
	}

	return true
}

func (server *MailServer) shouldForwardEmail(to string) bool {
	return !server.ForwardHostFilter || strings.Contains(to, server.ForwardHost)
}

func isEmailAddressesValid(mc *model.Mail) error {
	if from, err := CleanupEmail(mc.From); err == nil {
		mc.From = from
	} else {
		log.Println("From is not valid")
		return err
	}
	if to, err := CleanupEmail(mc.To); err == nil {
		mc.To = to
	} else {
		log.Println("To is not valid: ", mc.To)
		return err
	}
	return nil
}
func CleanupEmail(str string) (email string, err error) {
	str = strings.TrimSpace(str)
	address, err := mail.ParseAddress(str)
	if err != nil {
		return "", err
	}
	return address.Address, nil
}
