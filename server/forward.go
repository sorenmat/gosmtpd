package server

import (
	"log"
	"net"
	"net/smtp"

	"gitlab.com/sorenmat/gosmtpd/model"
)

func (server *MailServer) getForwardHost() string {
	return net.JoinHostPort(*&server.ForwardHost, *&server.ForwardPort)
}
func (server *MailServer) ForwardEmail(client *model.Mail) {
	log.Printf("Forwarding mail to: %v using host %v ", client.To, server.getForwardHost())

	err := smtp.SendMail(server.getForwardHost(), server.getAuth(), client.From, []string{client.To}, []byte(client.Data))
	if err != nil {
		log.Println("Forward error: ", err)
	}
}

func (server *MailServer) getAuth() smtp.Auth {
	if *&server.ForwardUser == "" && *&server.ForwardPassword == "" {
		return nil
	}
	auth := smtp.PlainAuth(
		"",
		server.ForwardUser,
		server.ForwardPassword,
		server.ForwardHost,
	)
	return auth

}
