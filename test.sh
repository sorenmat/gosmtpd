#!/bin/bash

# Mail server details
MAIL_SERVER="localhost"
PORT="2525"

# Email details
SENDER="sender@example.com"
RECIPIENT="recipient@example.com"
SUBJECT="Your Subject Here"
BODY="Hello,

This is the body of the email.

Regards,
Your Name"

# Telnet commands
{
    sleep 1
    echo "EHLO example.com"
    sleep 1
    echo "MAIL FROM: <$SENDER>"
    sleep 1
    echo "RCPT TO: <$RECIPIENT>"
    sleep 1
    echo "DATA"
    sleep 1
    echo "Subject: $SUBJECT"
    echo "From: Sender Name <$SENDER>"
    echo "To: Recipient Name <$RECIPIENT>"
    echo
    echo "$BODY"
    echo "."
    sleep 1
    echo "QUIT"
} | telnet $MAIL_SERVER $PORT
