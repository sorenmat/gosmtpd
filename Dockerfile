FROM alpine
LABEL MAINTAINER="Soren Mathiasen <sorenm@mymessages.dk>"

RUN apk update && apk add ca-certificates

ADD gosmtpd /
EXPOSE 2525
EXPOSE 8080
CMD ["/gosmtpd"]
