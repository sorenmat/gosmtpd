package inmem

import (
	"fmt"
	"log"
	"sync"
	"time"

	"gitlab.com/sorenmat/gosmtpd/model"
)

// InMemDB holds the in-memory representation of the mails
type InMemDB struct {
	mu    sync.Mutex
	items []model.Mail
}

// NewDb creates an initialized struct
func NewDb() *InMemDB {
	return &InMemDB{}
}

func (d *InMemDB) Save(item model.Mail) {
	d.mu.Lock()
	defer d.mu.Unlock()
	d.items = append(d.items, item)
}

func (d *InMemDB) Delete(email string) {
	d.mu.Lock()
	defer d.mu.Unlock()
	var result []model.Mail
	for _, msg := range d.items {
		if msg.To != email {
			result = append(result, msg)
		}
	}
	d.items = result
}

func (d *InMemDB) DeleteByID(id string) {
	var result []model.Mail
	d.mu.Lock()
	defer d.mu.Unlock()
	for _, msg := range d.items {
		if msg.MailId != id {
			result = append(result, msg)
		}
	}
	d.items = result
}

func (d *InMemDB) Empty() {
	d.mu.Lock()
	d.items = []model.Mail{}
	d.mu.Unlock()
}

func (d *InMemDB) Get(id string) (model.Mail, error) {
	d.mu.Lock()
	defer d.mu.Unlock()
	for _, msg := range d.items {
		if msg.MailId == id {
			return msg, nil
		}
	}

	return model.Mail{}, fmt.Errorf("unable to find ID with %v", id)
}

func (d *InMemDB) Find(email string) []model.Mail {
	//d.items = append(d.items, item)
	var result []model.Mail
	d.mu.Lock()
	defer d.mu.Unlock()

	for _, msg := range d.items {
		if msg.To == email {
			result = append(result, msg)
		}
	}

	return result
}

func (d *InMemDB) Cleanup() {
	d.mu.Lock()
	defer d.mu.Unlock()
	log.Printf("About to expire entries from database, current length %d\n", len(d.items))

	dbcopy := []model.Mail{}
	for _, v := range d.items {
		if time.Since(v.ExpireStamp).Seconds() < 0 {
			dbcopy = append(dbcopy, v)
		}
	}
	d.items = dbcopy
	log.Println("After expire entries in database, new length ", len(d.items))

}

func (d *InMemDB) Size() int {
	return len(d.items)
}

func (d *InMemDB) All() []model.Mail {
	return d.items
}
