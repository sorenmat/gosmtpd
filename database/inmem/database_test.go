package inmem

import (
	"testing"

	"gitlab.com/sorenmat/gosmtpd/model"
)

func TestSave(t *testing.T) {
	db := NewDb()
	db.Save(model.Mail{From: "joe@void.dk", To: "the_joker@batman.inc"})
	if db.Size() != 1 {
		t.Error("Save doesn't work")
	}
	db.Save(model.Mail{From: "joe@void.dk", To: "the_joker@batman.inc"})
	if db.Size() != 2 {
		t.Error("Save doesn't work")
	}
}

func TestGetByID(t *testing.T) {
	db := NewDb()
	db.Save(model.Mail{MailId: "1", From: "joe@void.dk", To: "the_joker@batman.inc"})
	m, err := db.Get("1")
	if err != nil {
		t.Error("Expected a mail with id 1")
	}
	if m.MailId != "1" {
		t.Error("Expected a mail with id 1")
	}
}

func TestGetByIDWithUnkownId(t *testing.T) {
	db := NewDb()
	db.Save(model.Mail{MailId: "1", From: "joe@void.dk", To: "the_joker@batman.inc"})
	m, err := db.Get("2")
	if err == nil {
		t.Error("Didn't expected a mail with id 1")
	}
	emptyMail := model.Mail{}
	if m != emptyMail {
		t.Error("Didn't expected a mail with id 1")

	}
}

func TestGetByEmail(t *testing.T) {
	db := NewDb()
	db.Save(model.Mail{MailId: "1", From: "joe@void.dk", To: "the_joker@batman.inc"})
	m := db.Find("the_joker@batman.inc")
	if len(m) != 1 {
		t.Error("Expected one mail")
	}
	if m[0].MailId != "1" {
		t.Error("Expected a mail with id 1")
	}
	db.Save(model.Mail{MailId: "2", From: "joe@void.dk", To: "the_joker@batman.inc"})
	m = db.Find("the_joker@batman.inc")
	if len(m) != 2 {
		t.Error("Expected two mails")
	}

}

func TestGetByEmailWithUnkownEmail(t *testing.T) {
	db := NewDb()
	db.Save(model.Mail{MailId: "1", From: "joe@void.dk", To: "the_joker@batman.inc"})
	m := db.Find("nobody@inc.com")
	if len(m) != 0 {
		t.Error("Didn't expect to find anymails")
	}
}

func TestEmptyDatabase(t *testing.T) {
	db := NewDb()
	db.Save(model.Mail{MailId: "1", From: "joe@void.dk", To: "the_joker@batman.inc"})
	db.Empty()
	m := db.Find("the_joker@batman.inc")
	if len(m) != 0 {
		t.Error("Didn't expect to find anymails")
	}
}

func TestDeleteById(t *testing.T) {
	db := NewDb()
	db.Save(model.Mail{MailId: "1", From: "joe@void.dk", To: "the_joker@batman.inc"})
	db.DeleteByID("1")
	m := db.Find("the_joker@batman.inc")
	if len(m) != 0 {
		t.Error("Didn't expect to find anymails: ", m)
	}
}
