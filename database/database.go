package database

import "gitlab.com/sorenmat/gosmtpd/model"

type DataStore interface {
	Save(item model.Mail)
	Delete(email string)
	DeleteByID(id string)
	Empty()
	Get(id string) (model.Mail, error)
	Find(email string) []model.Mail
	Cleanup()
	Size() int
	All() []model.Mail
}
