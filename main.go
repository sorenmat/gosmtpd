package main

import (
	"bufio"
	"log"
	"net"
	"sync"

	"time"

	"github.com/alecthomas/kingpin/v2"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/sorenmat/gosmtpd/database/inmem"
	"gitlab.com/sorenmat/gosmtpd/model"
	"gitlab.com/sorenmat/gosmtpd/server"
)

const timeout time.Duration = time.Duration(10)
const mailMaxSize = 1024 * 1024 * 2 // 2 MB

var debug = kingpin.Flag("debug", "Enable debug mode, this will dump mails to the console").OverrideDefaultFromEnvar("DEBUG").Bool()

var webport = kingpin.Flag("webport", "Port the web server should run on").Default("8000").OverrideDefaultFromEnvar("WEBPORT").String()
var hostname = kingpin.Flag("hostname", "Hostname for the smtp server to listen to").Default("localhost").String()
var port = kingpin.Flag("port", "Port for the smtp server to listen to").Default("2525").String()
var ssl = kingpin.Flag("ssl", "Should the web server enable https only").Default("false").OverrideDefaultFromEnvar("USE_SSL_ONLY").Bool()

var sslcert = kingpin.Flag("cert", "Certificate").String()
var sslkey = kingpin.Flag("key", "Private key").String()

var forwardhostfilter = kingpin.Flag("forwardhostfilter", "If the filter in the hostname should be applied for the outgoing emails or not").Default("true").OverrideDefaultFromEnvar("FORWARD_HOST_FILTER").Bool()
var forwardhost = kingpin.Flag("forwardhost", "The hostname after the @ that we should forward i.e. gmail.com").Default("").OverrideDefaultFromEnvar("FORWARD_HOST").String()
var forwardsmtp = kingpin.Flag("forwardsmtp", "SMTP server to forward the mail to").Default("").OverrideDefaultFromEnvar("FORWARD_SMTP").String()
var forwardport = kingpin.Flag("forwardport", "The port on which email should be forwarded").Default("25").OverrideDefaultFromEnvar("FORWARD_PORT").String()
var forwarduser = kingpin.Flag("forwarduser", "The username for the forward host").Default("").OverrideDefaultFromEnvar("FORWARD_USER").String()
var forwardpassword = kingpin.Flag("forwardpassword", "Password for the user").Default("").OverrideDefaultFromEnvar("FORWARD_PASSWORD").String()

var cleanupInterval = kingpin.Flag("mailexpiration", "Time in seconds for a mail to expire, and be removed from database").Default("300").Int()

func createListener(server *server.MailServer) net.Listener {
	addr := "0.0.0.0:" + server.Port
	listener, err := net.Listen("tcp", addr)
	if err != nil {
		panic(err)
	} else {
		log.Println("Listening on tcp ", addr)
	}
	return listener
}

func serve(mailserver *server.MailServer) {
	server := server.NewMailServer(*mailserver)
	if server.Httpport == "" {
		log.Fatal("HTTPPort needs to be configured")
	}
	server.Database = inmem.NewDb()
	server.ExpireMutex = &sync.Mutex{}
	go func() {
		for {
			server.Database.Cleanup()
			server.ExpireMutex.Lock()
			i := server.Expireinterval
			server.ExpireMutex.Unlock()
			time.Sleep(time.Duration(i) * time.Second)
		}
	}()

	e := echo.New()
	setupWebRoutes(server, e)

	listener := createListener(server)
	go func() {
		for {
			conn, err := listener.Accept()
			if err != nil {
				log.Panicf("Unable to accept: %s\n", err)
				continue
			}
			go processClientRequest(&model.MailConnection{
				//Mailserver: mailserver,
				Connection: conn,
				Address:    conn.RemoteAddr().String(),
				Reader:     bufio.NewReader(conn),
				Writer:     bufio.NewWriter(conn),
				Mail:       model.Mail{MailId: uuid.New().String(), Received: time.Now().Unix()},
			}, server)
		}
	}()
	log.Println("Trying to bind web to port ", server.Httpport)

	if server.Ssl {

		log.Fatal(e.StartTLS(":"+server.Httpport, server.Sslcert, server.Sslkey))
	} else {
		log.Fatal(e.Start(":" + server.Httpport))
	}
}

func main() {
	kingpin.Parse()
	forwardEnabled := false
	if *forwardhost != "" && *forwardport != "" {
		forwardEnabled = true
	}

	if *ssl {
		if *sslcert == "" || *sslkey == "" {
			log.Fatal("When SSL is enabled both a certificate and private key is required")
		}
	}

	serve(server.NewMailServer(server.MailServer{Hostname: *hostname,
		Port:              *port,
		Httpport:          *webport,
		ForwardEnabled:    forwardEnabled,
		ForwardHostFilter: *forwardhostfilter,
		ForwardHost:       *forwardhost,
		ForwardPort:       *forwardport,
		Expireinterval:    *cleanupInterval,
		Ssl:               *ssl,
		Sslcert:           *sslcert,
		Sslkey:            *sslkey,
		Debug:             *debug,
	}))
}
