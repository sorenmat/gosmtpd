package model

import (
	"bufio"
	"io"
	"net"
	"time"
)

const timeout time.Duration = time.Duration(10)

// Mail is the basic structure of the mail, this is used for serializing the mail to the storage
type Mail struct {
	MailId                           string
	To, From, Subject, Data, CC, Bcc string
	Received                         int64

	// when should this mail be clean up
	ExpireStamp time.Time
}

// MailConnection is used when a connection is made to the server, the server then create a MailConnection, to keep track
// of the specific client connection
type MailConnection struct {
	Mail
	Recepient []string
	State     State
	Helo      string
	Response  string
	Address   string

	Connection     net.Conn
	Reader         *bufio.Reader
	Writer         *bufio.Writer
	DropConnection bool
	//Mailserver     *server.MailServer
}

// State representating the flow of the connection
type State int

const (
	INITIAL   State = iota
	NORMAL    State = iota
	READ_DATA State = iota
)

const (
	OK        = "250 OK"
	EHLO      = "EHLO"
	NO_OP     = "NOOP"
	HELLO     = "HELO"
	SUBJECT   = "SUBJECT: "
	DATA      = "DATA"
	MAIL_FROM = "MAIL FROM:"
	RCPT_TO   = "RCPT TO:"
	RESET     = "RSET"
)

func (mc *MailConnection) ResetDeadLine() {
	err := mc.Connection.SetDeadline(time.Now().Add(timeout * time.Second))
	if err != nil {
		panic(err)
	}
}

func (mc *MailConnection) NextLine() (string, error) {
	return mc.Reader.ReadString('\n')
}

func (mc *MailConnection) InDataMode() bool {
	return mc.State == READ_DATA
}

// Flush the response buffer to the n
func (mc *MailConnection) Flush() bool {
	mc.ResetDeadLine()
	size, err := mc.Writer.WriteString(mc.Response)
	mc.Writer.Flush()
	mc.Response = mc.Response[size:]

	if err != nil {
		if err == io.EOF {
			// connection closed
			return false
		}
		if neterr, ok := err.(net.Error); ok && neterr.Timeout() {
			// a timeout
			return false
		}
	}
	if mc.DropConnection {
		return false
	}
	return true
}
