package main

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"testing"
	"time"

	"gitlab.com/sorenmat/gosmtpd/model"
	"golang.org/x/net/websocket"
)

func ws_client(ws_url string) *websocket.Conn {
	roots := x509.NewCertPool()
	rootPEM, err := ioutil.ReadFile(mailconfig.Sslcert)
	if err != nil {
		log.Fatal("Unable to load root pem")
	}
	ok := roots.AppendCertsFromPEM([]byte(rootPEM))
	if !ok {
		panic("failed to parse root certificate")
	}
	tlsConf := &tls.Config{
		RootCAs: roots,
	}
	u, _ := url.Parse(ws_url)
	origin, _ := url.Parse("http://localhost/")
	conn, err := websocket.DialConfig(&websocket.Config{
		TlsConfig: tlsConf,
		Location:  u,
		Origin:    origin,
		Version:   websocket.ProtocolVersionHybi,
	})
	if err != nil {
		log.Fatal("Unable to create secure websocket connection: ", err)
	}
	return conn
}

func TestRegEx(t *testing.T) {
	r := regexp.MustCompile(`wss://.*/inbox/.*/listen`)

	if !r.MatchString("wss://localhost:8000/inbox/sorenm@test.com/listen") {
		t.Error("URL should have been matched")
	}
	if r.MatchString("http://localhost:8000/inbox/sorenm@test.com/12345") {
		t.Error("URL should have been matched")
	}

}

var WS_BASE_URL = "wss://localhost:8000"

// While listing to the web socket we should only receive mails we have subscribed on.
func TestWebSocket(t *testing.T) {
	email := "sorenm@test.com"
	ws := ws_client(WS_BASE_URL + "/inbox/" + email + "/listen")
	defer ws.Close()

	sendMail(email)

	var message string
	websocket.Message.Receive(ws, &message)
	mc := &model.MailConnection{}
	json.Unmarshal([]byte(message), mc)

	if mc.To != email {
		t.Error("Email contains wrong 'to' address")
	}
	if mc.From != "sorenm@mymessages.dk" {
		t.Error("Email contains wrong 'from' address")
	}

}

// While listing to the web socket we should only receive mails we have subscribed on.
// We should not see email for other addresses
func TestWebSocket_filter(t *testing.T) {
	email := "sorenm@test.com"
	ws := ws_client(WS_BASE_URL + "/inbox/" + email + "/listen")
	defer ws.Close()

	sendMail("joe@void.com")
	sendMail(email)
	sendMail("joe@void1.com")

	var message string
	websocket.Message.Receive(ws, &message)
	mc := &model.MailConnection{}
	json.Unmarshal([]byte(message), mc)

	if mc.To != email {
		t.Error("Email contains wrong 'to' address")
	}
	if mc.From != "sorenm@mymessages.dk" {
		t.Error("Email contains wrong 'from' address")
	}

}

// Listen to websocket, but don't send any mails.
// This should result in a read time out
func TestWebSocket_NoSend(t *testing.T) {
	email := "sorenm@test.com"
	ws := ws_client(WS_BASE_URL + "/inbox/" + email + "/listen")
	defer ws.Close()

	var message string
	ws.SetReadDeadline(time.Now().Add(2 * time.Second))
	err := websocket.Message.Receive(ws, &message)

	if err == nil {
		t.Error("Get a message, expected none. ", err)
	}

	if message != "" {
		t.Error("Get a message, expected none")
	}
}

// While listing to the web socket we should only receive mails we have subscribed on.
func TestWebSocket_Since(t *testing.T) {
	email := "sorenm@test.com"
	beforeMails := time.Now().Unix()
	sendMailWithBody(email, "Subject: Mail 1\r\nBody")
	sendMailWithBody(email, "Subject: Mail 2\r\nBody")
	sendMailWithBody(email, "Subject: Mail 3\r\nBody")

	ws := ws_client(WS_BASE_URL + "/inbox/" + email + "/listen?since=" + strconv.FormatInt(beforeMails, 10))
	defer ws.Close()

	var message string
	for i := 0; i < 3; i++ {
		websocket.Message.Receive(ws, &message)
		mc := &model.Mail{}
		json.Unmarshal([]byte(message), mc)
		if message == "" {
			t.Error("Expected a message")
		}
		if mc.Subject != fmt.Sprintf("Mail %v", (i+1)) {
			t.Error("Subject of mail was wrong")
		}
		if mc.To != email {
			t.Error("Email contains wrong 'to' address")
		}
		if mc.From != "sorenm@mymessages.dk" {
			t.Error("Email contains wrong 'from' address")
		}
	}

}

func TestHTMLEncoding(t *testing.T) {
	emails := []model.Mail{
		model.Mail{
			"1",
			"test@test.com",
			"test@test.com",
			"This is a test",
			"Subject: Testing\r\nContent-Type: text/html; charset='UTF-8'\r\nContent-Transfer-Encoding: quoted-printable\r\n\r\n<div></div>",
			"",
			"",
			1,
			time.Now(),
		},
	}

	result := toHTML(emails)

	if !strings.Contains(result, "&lt;div&gt;&lt;/div&gt;") {
		t.Error("Email content wasn't encoded")
	}
}
