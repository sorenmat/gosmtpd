package main

import (
	"bytes"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/smtp"
	"strings"
	"testing"
	"time"

	"gitlab.com/sorenmat/gosmtpd/model"
	"gitlab.com/sorenmat/gosmtpd/server"
)

var mailconfig = createmailconfig()

func createmailconfig() *server.MailServer {
	return server.NewMailServer(server.MailServer{Port: "2525", Httpport: "8000",
		ForwardEnabled: false, Expireinterval: 1, Ssl: true,
		Sslcert: "example.crt", Sslkey: "example.key"})
}

var BASE_URL = "https://localhost:8000"

func client() *http.Client {
	roots := x509.NewCertPool()
	rootPEM, err := ioutil.ReadFile(mailconfig.Sslcert)
	if err != nil {
		log.Fatal("Unable to load root pem")
	}
	ok := roots.AppendCertsFromPEM([]byte(rootPEM))
	if !ok {
		panic("failed to parse root certificate")
	}
	tlsConf := &tls.Config{RootCAs: roots}
	tr := &http.Transport{TLSClientConfig: tlsConf}
	return &http.Client{Transport: tr}
}

func init() {
	go serve(mailconfig)
	for {

		resp, err := client().Get(BASE_URL + "/status")
		if err == nil && resp.StatusCode == 200 {
			break
		}
		time.Sleep(100 * time.Millisecond)
	}
}

func BenchmarkSendMails(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sendMailWithBody("sorenbench@test.com", "message")

	}
	d, _ := getMails("sorenbench@test.com")
	if b.N != len(d) {
		b.Errorf("Wrong number of email expected %d got %d\n", b.N, len(d))
	}
}

func TestStatusResource(t *testing.T) {
	resp, _ := client().Get(BASE_URL + "/status")
	if resp.StatusCode != 200 {
		t.Error("Server should respond with status code 200")
	}
	body, _ := ioutil.ReadAll(resp.Body)
	if string(body) != "OK" {
		t.Errorf("Expected body to be ok but was '%s'\n", string(body))
	}
}

func TestSendingMailWithMultilines(t *testing.T) {
	deleteRequest(BASE_URL + "/inbox")
	//	PORT = getPort() // we need to force this, since we don't parse the commandline
	emailAddress := randomEmail()
	sendMailWithBody(emailAddress, fmt.Sprintf("This\nis\na\ntest"))

	d, _ := getMails(emailAddress)

	if len(d) != 1 {
		t.Error("To many email ", len(d))
	}
}

func TestSendingMail(t *testing.T) {
	deleteRequest(BASE_URL + "/inbox")
	email := randomEmail()
	sendMail(email)
	d, sc := getMails(email)
	if sc != 200 {
		t.Error("Wrong status code expected 200 got", sc)
	}
	if len(d) != 1 {
		t.Error("To many email, ", len(d))
	}
	getEmailByHash(email, d[0].MailId)
}

func TestSendingMailWithBCC(t *testing.T) {
	deleteRequest(BASE_URL + "/inbox")
	email1 := randomEmail()
	email2 := randomEmail()
	sendMails([]string{email1, email2})
	d, _ := getMails(email1)

	if len(d) != 1 {
		t.Error("To many email expected 1 was ", len(d), d)
	}
	d, _ = getMails(email2)

	if len(d) != 1 {
		t.Error("To many email")
	}
}

func TestNoMailShouldHaveTheSameID(t *testing.T) {
	deleteRequest(BASE_URL + "/inbox")
	sendMails([]string{"sorenm@test.com", "joe@hat.com"})
	d, _ := getMails("sorenm@test.com")
	joesmail, _ := getMails("joe@hat.com")

	if d[0].MailId == joesmail[0].MailId {
		t.Error("No email should have the same ID", d[0].MailId, joesmail[0].MailId)
	}
}

func TestSendingMailSerialize(t *testing.T) {
	deleteRequest(BASE_URL + "/inbox")
	sendMail("sorenm@test.com")
	d, _ := getMails("sorenm@test.com")
	data, err := json.Marshal(d[0])
	if err != nil || data == nil {
		t.Error("Marshal error")
	}

	var f interface{}
	err = json.Unmarshal(data, &f)
	if err != nil {
		t.Error()
	}
	m := f.(map[string]interface{})
	if m["To"] != "sorenm@test.com" {
		t.Error()
	}
	if m["From"] != "sorenm@mymessages.dk" {
		t.Error()
	}
	if m["Subject"] != "Testing" {
		t.Error()
	}
	if m["Data"] != "Subject: Testing\r\nThis is $the email body.\r\nAnd it is the bomb\r\n.\r\n" {
		t.Error()
	}
	if m["CC"] != "" {
		t.Error()
	}
	if m["Bcc"] != "" {
		t.Error()
	}
	if m["Received"] == "" {
		t.Error()
	}
	id := fmt.Sprintf("%v", m["MailId"])
	if m["MailId"] == "" || !strings.Contains(id, "-") {
		// empty and a UUID
		t.Error()
	}

}
func isMailBoxSize(email string, size int, t *testing.T) bool {
	d, _ := getMails(email)
	if len(d) != size {
		t.Errorf("Wrong number of emails, expected %d but was %d", size, len(d))
		return false
	}
	return true
}

func TestSendingMailsToMultipleReceivers(t *testing.T) {
	deleteRequest(BASE_URL + "/inbox")
	sendMails([]string{"meet@test.com", "joe@test.com", "black@test.com"})

	isMailBoxSize("meet@test.com", 1, t)

	isMailBoxSize("joe@test.com", 1, t)
	isMailBoxSize("black@test.com", 1, t)

}

func TestSendingMailsToMultipleReceiversAndDeletingThem(t *testing.T) {
	deleteRequest(BASE_URL + "/inbox")
	sendMails([]string{"meet1@test.com", "joe1@test.com", "black1@test.com"})
	isMailBoxSize("meet1@test.com", 1, t)
	emptyMailBox("meet1@test.com")
	isMailBoxSize("meet1@test.com", 0, t)

	isMailBoxSize("joe1@test.com", 1, t)
	emptyMailBox("joe1@test.com")
	isMailBoxSize("joe1@test.com", 0, t)

	isMailBoxSize("black1@test.com", 1, t)
	emptyMailBox("black1@test.com")
	isMailBoxSize("black1@test.com", 0, t)
}

func TestSendingMailsToMultipleReceiversAndDeletingById(t *testing.T) {
	deleteRequest(BASE_URL + "/inbox")
	sendMails([]string{"meet2@test.com", "joe2@test.com", "black1@test.com"})

	mail, _ := getMails("meet2@test.com")
	if len(mail) != 1 {
		t.Error("Wrong number of emails")
	}
	deleteEmailByID("meet2@test.com", mail[0].MailId)

	mails, _ := getMails("meet2@test.com")
	if len(mails) != 0 {
		t.Error("Should be empty, but was ", len(mails))
	}

	mails, statusCode := getMails("joe2@test.com")
	if statusCode == 404 {
		t.Error("Was empty, but shouldn't be")
	}

}

func TestSendingMailAndDeletingIt(t *testing.T) {
	email := randomEmail()
	sendMail(email)
	d, _ := getMails(email)

	if len(d) != 1 {
		t.Error("Expected one email got ", len(d))
	}

	deleteEmailByID(email, d[0].MailId)
	d, _ = getMails(email)

	if len(d) != 0 {
		t.Errorf("Not the correct number '%d' of emails\n ", len(d))
	}

}

func TestMailExpiry(t *testing.T) {
	deleteRequest(BASE_URL + "/inbox")
	old := mailconfig.Expireinterval
	defer func() {
		mailconfig.ExpireMutex.Lock()
		mailconfig.Expireinterval = old
		mailconfig.ExpireMutex.Unlock()
	}()

	getAllMails()

	mailconfig.ExpireMutex.Lock()
	mailconfig.Expireinterval = 1
	mailconfig.ExpireMutex.Unlock()

	for i := 0; i < 100; i++ {
		email := randomEmail()
		sendMail(email)
	}

	time.Sleep(2 * time.Second)
	afterMails, _ := getAllMails()
	if len(afterMails) != 0 {
		t.Error("All mails should have expired but found ", len(afterMails))
	}
}

func TestGettingANonExistingInbox(t *testing.T) {
	deleteRequest(BASE_URL + "/inbox")
	email := randomEmail()
	_, status := getMails(email)
	if status != 404 {
		t.Error("Should return 404")
	}

}

func TestGettingANonExistingId(t *testing.T) {
	deleteRequest(BASE_URL + "/inbox")
	email := randomEmail()
	_, status := getEmailByHash(email, email)
	if status != 404 {
		t.Error("Should return 404, but was ", status)
	}

}

func getEmailByHash(email, hash string) (model.Mail, int) {
	resp, err := client().Get(BASE_URL + "/inbox/" + email + "/" + hash)
	if err != nil {
		log.Fatal("Unable to call emailByHash service: ", err)
	}
	if resp.StatusCode != 200 {
		return model.Mail{}, resp.StatusCode
	}
	data, _ := ioutil.ReadAll(resp.Body)

	decoder := json.NewDecoder(bytes.NewReader(data))
	var d model.Mail
	err = decoder.Decode(&d)
	if err != nil {
		log.Fatal("getEmailByHash failed: ", err)
	}

	return d, resp.StatusCode
}

func deleteRequest(url string) (*http.Response, error) {
	client := client()
	req, err := http.NewRequest(
		"DELETE",
		url,
		bytes.NewBuffer([]byte("")),
	)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	reply, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	if reply.StatusCode != 204 {
		log.Fatal("Delete request failed ", url)
	}
	return reply, err
}

// Delete a specific mail by finding via the id
func deleteEmailByID(email, hash string) {
	deleteRequest(BASE_URL + "/inbox/" + email + "/" + hash)
}

// Delete all mails in an inbox
func emptyMailBox(email string) {
	deleteRequest(BASE_URL + "/inbox/" + email)
}

func sendMails(receiver []string) {
	err := smtp.SendMail("localhost:2525",
		nil,
		"sorenm@mymessages.dk", // sender
		receiver,               //recipient
		[]byte("Subject: Testing\nThis is $the email body.\nAnd it is the bomb"),
	)
	if err != nil {
		log.Fatal(err)
	}
}

func sendMail(receiver string) {
	err := smtp.SendMail("localhost:2525",
		nil,
		"sorenm@mymessages.dk", // sender
		[]string{receiver},     //recipient
		[]byte("Subject: Testing\nThis is $the email body.\nAnd it is the bomb"),
	)
	if err != nil {
		log.Fatal(err)
	}
}

func sendMailWithBody(receiver string, msg string) {
	err := smtp.SendMail("localhost:2525",
		nil,
		"sorenm@mymessages.dk", // sender
		[]string{receiver},     //recipient
		[]byte(msg),
	)
	if err != nil {
		log.Fatal(err)
	}
}

func TestForwardHostname(t *testing.T) {
	host := "something.com"
	port := "2525"
	result := net.JoinHostPort(host, port)
	if result != "something.com:2525" {
		t.Error("Expected something.com:2525 got ", result)
	}
}

func TestForwardHostnameWithoutPort(t *testing.T) {
	host := "something.com"
	port := ""
	result := net.JoinHostPort(host, port)
	if result != "something.com:" {
		t.Error("Expected 'something.com:' got ", result)
	}
}

func TestEmail(t *testing.T) {
	emails := []string{"test@something.com", "  test@something.com", " test@something.com        "}
	for _, email := range emails {
		name, err := server.CleanupEmail(email)
		if name != "test@something.com" {
			t.Error("Expected 'test@something.com' got ", name)
		}
		if err != nil {
			t.Error("Cleanup email resulted in an error ", err)
		}
	}
}

func TestExtractSubjectOnSingleLine(t *testing.T) {
	mc := &model.MailConnection{}
	line := `SUBJECT: Testing`
	scanForSubject(&mc.Mail, line)
	if mc.Subject != "Testing" {
		t.Error("Expected 'Testing' got ", mc.Subject)
	}
}
func TestExtractSubjectOnSingleNotFormattedLine(t *testing.T) {
	mc := &model.MailConnection{}
	line := `SUBJECT:             Testing                  `
	scanForSubject(&mc.Mail, line)
	if mc.Subject != "            Testing                  " {
		t.Error("Exptected '            Testing                  ' got ", mc.Subject)
	}
}

func TestExtractSubjectWithNewLine(t *testing.T) {
	mc := &model.MailConnection{}
	line := `SUBJECT: 
Testing`
	scanForSubject(&mc.Mail, line)
	if mc.Subject != "\nTesting" {
		t.Error(mc.Subject, len(mc.Subject))
	}
}

func TestExtractSubjectOnMultipleLines(t *testing.T) {
	mc := &model.MailConnection{}
	line := `SUBJECT: 
Testing
Multiline subject`
	scanForSubject(&mc.Mail, line)
	if mc.Subject != "\nTesting\nMultiline subject" {
		t.Error(mc.Subject, len(mc.Subject))
	}
}

func randomEmail() string {
	dictionary := "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

	var bytes = make([]byte, 10)
	_, err := rand.Read(bytes)
	if err != nil {
		panic(err)
	}
	for k, v := range bytes {
		bytes[k] = dictionary[v%byte(len(dictionary))]
	}
	return string(bytes) + "@test.com"
}

func getMails(email string) ([]model.MailConnection, int) {
	resp, err := client().Get(BASE_URL + "/inbox/" + email)
	if err != nil {
		log.Fatal(err)
	}
	var d []model.MailConnection
	if resp.StatusCode == 404 {
		return d, resp.StatusCode
	}
	//x, _ := httputil.DumpResponse(resp, true)
	//fmt.Println("Response: ", string(x))
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&d)
	if err != nil {
		log.Println(d)
		log.Fatal(err)
	}

	return d, resp.StatusCode
}

func getAllMails() ([]model.MailConnection, int) {
	resp, _ := client().Get(BASE_URL + "/inbox")

	decoder := json.NewDecoder(resp.Body)
	var d []model.MailConnection
	decoder.Decode(&d)
	return d, resp.StatusCode
}
